import React,{Component} from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'
import Register from './Register'
import Login from './Login'
import Main from './Main'

const AppNavigator=createStackNavigator(
    {
      Register:Register,
      Login:Login,
      Main:Main
    },
    {
        initialRouteName:'Login',
        defaultNavigationOptions:{
            headerTitleAlign:'center'
        }
    }
);

const AppContainer=createAppContainer(AppNavigator);
export default class App extends React.Component{
    render(){
        return <AppContainer/>;
    }
}
