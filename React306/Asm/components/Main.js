import React, { useCallback } from 'react'
import { View, Text } from 'react-native'
import { createNavigator, TabRouter } from 'react-navigation'
import BottomNavigation, {
  FullTab
} from 'react-native-material-bottom-navigation'
import Icon from 'react-native-vector-icons/FontAwesome'

// Screens. Normally you would put these in separate files.
import User from './User'
import Maps from './Maps'
import Products from './Products'




function AppTabView(props) {
  const tabs = [
    { key: 'User', label: 'User', barColor: '#ff9900', icon: 'user' },
    { key: 'Products', label: 'Products', barColor: '#6A1B9A', icon: 'shopping-bag' },
    { key: 'Maps', label: 'Maps', barColor: '#007700', icon: 'map' }
  ]

  const { navigation, descriptors } = props
  const { routes, index } = navigation.state
  const activeScreenName = routes[index].key
  const descriptor = descriptors[activeScreenName]
  const ActiveScreen = descriptor.getComponent()

  const handleTabPress = useCallback(
    newTab => navigation.navigate(newTab.key),
    [navigation]
  )

  return (
    <View style={{ flex: 1 }}>
      <View style={{ flex: 1 }}>
        <ActiveScreen navigation={descriptor.navigation} />
      </View>

      <BottomNavigation
        tabs={tabs}
        activeTab={activeScreenName}
        onTabPress={handleTabPress}
        renderTab={({ tab, isActive }) => (
          <FullTab
            isActive={isActive}
            key={tab.key}
            label={tab.label}
            renderIcon={() => <Icon name={tab.icon} size={24} color="white" />}
          />
        )}
      />
    </View>
  )
}

const AppTabRouter = TabRouter({
  User: { screen:User },
  Products: { screen: Products },
  Maps: { screen: Maps }
})

const AppNavigator = createNavigator(AppTabView, AppTabRouter, {})

export default AppNavigator