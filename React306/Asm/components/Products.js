import React, { Component } from 'react';
import {
    View, Text, TouchableOpacity, TextInput, FlatList, StyleSheet
} from 'react-native';
import database from '@react-native-firebase/database';

function Item({ data }) {
    return (
        <View >
            <Text >{data.name}</Text>
            <Text >{data.price}</Text>
        </View>
    );
}

export default class App extends Component {
    state = {
        dataFood: null,
        name: '',
        price: '',
        currId: ''
    }
    componentDidMount() {
        this.readFlowers();
    }
    async readFood() {
        // Create a reference
        const ref = database().ref('/hoa');

        // Fetch the data snapshot
        await ref.on('value', (snapshot) => {
            this.setState({ dataFood: Object.entries(snapshot.val()) });
            // this.setState({dataUsers:snapshot.toJSON()});

        });
        const snapshot = await ref.once('value');
        console.log('User data: ', Object.entries(snapshot.val()));


    }
    async addFlowers() {
        // Create a reference
        const ref = database().ref('/hoa');

        await ref.push({
            name: this.state.name,
            price: this.state.price,
        });
    }
    async updateFlowers() {
        var id = this.state.currId;
        console.log("currId: " + id);
        const ref = database().ref('/hoa/' + id);
        await ref.set({
            name: this.state.name,
            price: this.state.price,
        });
        this.setState({ name: '', price: '', currId: '' });
    }
    async deleteFlowers() {
        var id = this.state.currId;
        console.log("currId: " + id);
        const ref = database().ref('/hoa/' + id);
        await ref.remove();
        this.setState({ name: '', price: '', currId: '' });
    }
    render() {
        return (
            <View style={styles.container}>
                <TextInput style={styles.textInput}
                    onChangeText={(text) => this.setState({ name: text })}
                    value={this.state.name} />
                <TextInput style={styles.textInput}
                    onChangeText={(text) => this.setState({ price: text })}
                    value={this.state.price} />
                <TouchableOpacity style={styles.button} onPress={() => this.addFlowers()}>
                    <Text>Add</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => this.updateFlowers()}>
                    <Text>Update</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => this.deleteFlowers()}>
                    <Text>Delete</Text>
                </TouchableOpacity>
                <FlatList
                    data={this.state.dataFood}
                    renderItem={({ item }) =>
                        (
                            <TouchableOpacity onPress={() => this.setState({
                                name: item[1].name, price: item[1].price, currId: item[0]
                            })}>
                                <View style={styles.item} >
                                    <Text >{item[1].name}</Text>
                                    <Text >{item[1].price}</Text>
                                </View>
                            </TouchableOpacity>
                        )}
                    keyExtractor={item => item.key}
                />



            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 10
    },
    textInput:
    {
        borderColor: 'gray',
        borderWidth: 1,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#f194ff',
        padding: 10,
        marginVertical: 8,
        marginHorizontal: 16,
    },
})